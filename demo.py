import sounddevice as sd
import time
import numpy as np
import tensorflow as tf
from utils import list_from_txt, return_filepath_dictionary
from sklearn import preprocessing
import os

# train label encoder on training set
labels = ['four', 'learn', 'dog', 'marvin', 'five', 'sheila', 'bed', 'three', 'happy', 'left', 'right', 'go', 'off', 'on', 'no', 'follow', 'nine', 'tree', 'up', 'cat', 'yes', 'zero', 'eight', 'down', 'backward', 'house', 'stop', 'visual', 'seven', 'one', 'bird', 'forward', 'six', 'wow', 'two']
label_encoder = preprocessing.LabelEncoder()
label_encoder.fit(labels)

fs = 16000  # Sample rate
seconds = 1  # Duration of recording

print('Please say one of the following words in...\n')
print(labels)
time.sleep(1)
print("\n3")
time.sleep(1)
print("2")
time.sleep(1)
print("1")
time.sleep(1)
print("Recording...")
myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=1)
sd.wait()  # Wait until recording is finished
print("\nDone")

X = tf.convert_to_tensor(np.transpose(myrecording), dtype='float32')

# Load model
saved_model_filepath = os.path.join(os.getcwd(),'saved_models/speech_recognition_19')
model = tf.keras.models.load_model(saved_model_filepath)

logits = model.predict(X, use_multiprocessing=False, verbose=0)
y_predicted = tf.argmax(logits, axis=1)
print("\nDid you say \'{}\' ?".format(label_encoder.inverse_transform([y_predicted[0]])[0]))
