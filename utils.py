import os
import tensorflow as tf
from tensorflow.python.ops import io_ops
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft
import pickle
import time
from tqdm import tqdm
from scipy.signal import hilbert
from multiprocessing import Pool, cpu_count
from functools import partial
from sklearn import preprocessing
import time
from sklearn.metrics import accuracy_score, balanced_accuracy_score, matthews_corrcoef, recall_score, precision_score, confusion_matrix, f1_score
import json

def list_from_txt(txt_filepath):
	'''loads contents of txt file as list'''
	with open(txt_filepath, "r") as f:
		list_of_contents = []
		for item in f:
			list_of_contents.append(item[:-1])
	return list_of_contents

def compile_training_list(data_dir, validation_list, testing_list):
	'''compiles list of files not in val or test list to create training list'''
	training_list = []
	for root, _, files in os.walk(data_dir):
		for file in files:
			if not file.startswith('.'):
				label = root.split('/')[-1]
				label_filename = os.path.join(label, file)
				if label_filename not in testing_list or validation_list:
					training_list.append(label_filename)
	return training_list

def return_filepath_dictionary(dataset_list):
	'''
	accepts the file lists, e.g. list of all training relative filepaths ('right/0ba018fc_nohash_0.wav')
	return a dictionary whose keys are the classes and values are all relative filepaths
	'''
	# obtain classes
	label_set = set()
	for item in dataset_list:
		label = os.path.split(item)[0]
		label_set.add(label)

	# initialise dictionary
	filepath_dictionary = {}
	for label in label_set:
		filepath_dictionary[label] = []

	# append relative filepaths to list as value in class dictionary
	for item in dataset_list:
		label = os.path.split(item)[0]
		filepath_dictionary[label].append(item)

	return filepath_dictionary

def load_wav_file(filename, n_channels=1):
    '''
    loads wav file as numpy array using tensorflow
    '''
    wav_loader = io_ops.read_file(filename)
    wav_decoder = tf.audio.decode_wav(wav_loader, desired_channels=n_channels) # can pass desired_samples. pcm float between -1 and 1
    audio = wav_decoder.audio
    if n_channels==1:
        audio = tf.squeeze(audio)
    audio_np = audio.numpy()
    return audio_np

def plot_envelope(y, y_hilbert, filename):
    '''
    Plots training and validation accuracy vs epoch
    '''
    x = np.linspace(1, y.shape[0], y.shape[0])
    plt.plot(x, y, 'r', label='Original Waveform')
    plt.plot(x, y_hilbert, 'b', label='Envelope')
    plt.title('Waveform against Envelope')
    plt.legend(loc=0)
    plt.grid()
    plt.ylim(-1, 1)
    # plt.xlim(3000, 4000)
    plt.xlabel('desired_samples')
    plt.ylabel('Amplitude')
    plt.savefig(filename, dpi=300)
    plt.close()

def pad_audio(audio, max_len=16000):
	'''
	post pads 1D np audio
	'''
	padded_audio = np.zeros(max_len, dtype='float32')
	padded_audio[0:audio.shape[0]] = audio
	return padded_audio

def save_pickle(pickle_filename, data):
	with open(pickle_filename, 'wb') as f:
		pickle.dump(data, f)

def print_size(array_name, np_array):
		print("Size of {} is {} MB".format(array_name, int(np.round(np_array.nbytes/1024000, decimals = 0))))

def time_it(process, t):
    '''
    Times process and prints
    Input is start time of process using t = time.time()
    '''
    elapsed = time.time()-t
    minutes,seconds = np.divmod(elapsed,60)
    hours,minutes = np.divmod(minutes,60)
    if hours > 0:
        print("\nTime taken for {}: {} hours {} minutes {} seconds".format(process, int(hours), int(minutes), np.round(seconds, decimals=2)))
    elif minutes > 0:
        print("\nTime taken for {}: {} minutes {} seconds".format(process, int(minutes), np.round(seconds, decimals=2)))
    else:
        print("\nTime taken for {}: {} seconds".format(process, np.round(seconds, decimals=2)))

def generate_data_from_filepath_dictionary(filepath_dictionary, data_dir, label_encoder, data_type='raw_audio'):
	'''
	Creates numpy array of audio envelope data X and labels y
	from a class dictionary of key: labels, and value: list of filepaths
	data_type can be envelope or raw_audio
	'''
	label_list = list(filepath_dictionary.keys())
	m_total = 0
	ind_start = []
	ind_end = []

	for label in label_list:
		ind_start.append(m_total)
		m_total += len(filepath_dictionary[label])
		ind_end.append(m_total)

	# generate y
	y = np.zeros(m_total, dtype='float32')
	X = np.zeros((m_total, 16000, 1), dtype='float32')

	for label in tqdm(label_list):
		ind_label = label_list.index(label)
		m = len(filepath_dictionary[label]) # number of examples per class
		y_class = np.ones(m, dtype='float32') * label_encoder.transform([label]).astype('float32') # for this class only

		# generate X
		filepath_list = filepath_dictionary[label]
		X_class = np.zeros((len(filepath_list), 16000, 1), dtype='float32')

		for ind_file, rel_filepath in enumerate(filepath_list):
			filepath = os.path.join(data_dir, rel_filepath)
			audio_np = pad_audio(load_wav_file(filepath)) # shape (16000,)
			if data_type == 'envelope':
				envelope = np.abs(hilbert(audio_np))
				X_example = np.reshape(envelope,(1, envelope.shape[0], 1))
			elif data_type == 'raw_audio':
				X_example = np.reshape(audio_np,(1, audio_np.shape[0], 1))
			else:
				raise Exception('data_type should be envelope or raw_audio')
			X_class[ind_file, :, :] = X_example

		X[ind_start[ind_label]:ind_end[ind_label], :, :] = X_class
		y[ind_start[ind_label]:ind_end[ind_label]] = y_class

	return X, y

def smooth(x, window_len=11):
 
    s = np.r_[x[window_len-1:0:-1], x, x[-2:-window_len-1:-1]]

    window='hanning'
    w = eval('np.'+window+'(window_len)')

    y = np.convolve(w/w.sum(), s, mode='same')
    return y

def get_X_and_y_from_label(filepath_dictionary, data_dir, label_encoder, data_type, label_list, label):
	
	ind_label = label_list.index(label) 
	
	# generate y for this class
	m = len(filepath_dictionary[label]) # number of examples per class
	y_class = np.ones(m, dtype='float32') * label_encoder.transform([label]).astype('float32') # for this class only
	
	# generate X for this class
	filepath_list = filepath_dictionary[label]
	X_class = np.zeros((len(filepath_list), 16000, 1), dtype='float32')

	for ind_file, rel_filepath in enumerate(filepath_list):
		filepath = os.path.join(data_dir, rel_filepath)
		audio_np = pad_audio(load_wav_file(filepath)) # shape (16000,)
		if data_type == 'envelope':
			envelope = np.abs(hilbert(audio_np))
			X_example = np.reshape(envelope,(1, envelope.shape[0], 1))
		elif data_type == 'raw_audio':
			X_example = np.reshape(audio_np,(1, audio_np.shape[0], 1))
		else:
			raise Exception('data_type should be envelope or raw_audio')
		X_class[ind_file, :, :] = X_example

	return (X_class, y_class)

def generate_data_from_filepath_dictionary_multiprocessed(filepath_dictionary, data_dir, label_encoder, data_type='raw_audio'):
	'''
	Creates numpy array of audio envelope data X and labels y
	from a class dictionary of key: labels, and value: list of filepaths
	data_type can be envelope or raw_audio
	'''
	label_list = list(filepath_dictionary.keys())
	m_total = 0
	ind_start = []
	ind_end = []

	for label in label_list:
		ind_start.append(m_total)
		m_total += len(filepath_dictionary[label])
		ind_end.append(m_total)

	# generate y
	y = np.zeros(m_total, dtype='int32')
	X = np.zeros((m_total, 16000, 1), dtype='float32')

	(X_class, y_class) = get_X_and_y_from_label(filepath_dictionary, data_dir, label_encoder, data_type, label_list, label)
	
	get_X_and_y_from_label_partial = partial(get_X_and_y_from_label, filepath_dictionary, data_dir, label_encoder, data_type, label_list)
	n_proc = cpu_count()
	pool = Pool(n_proc)
	data_list = pool.map(get_X_and_y_from_label_partial, label_list)
	pool.close()
	pool.join()

	for ind_label, label in enumerate(label_list):
		(X_class, y_class) = data_list[ind_label]
		X[ind_start[ind_label]:ind_end[ind_label], :, :] = X_class
		y[ind_start[ind_label]:ind_end[ind_label]] = y_class
			
	return tf.cast(X, dtype = 'float32'), tf.cast(y, dtype='int32')

class LRN(tf.keras.layers.Layer): #LRN (derived class) inherits from Layers (base class)
    '''
    Custom layer to wrap local response normalisation function. Forces weights of adjacent layers to differ.
    Speeds up training / improves performance for given number of epochs.
    '''
    def __init__(self, **kwargs): # no other inputs required for Layers
        super().__init__(**kwargs)

    def call(self, inputs): 
        return tf.nn.lrn(inputs, depth_radius=2, bias=1, alpha=0.00002, beta=0.75, name='lrn')


def plot_accuracy_save(accuracy, val_accuracy, filename):
    '''
    Plots training and validation accuracy vs epoch
    '''
    epochs = range(1,len(accuracy)+1)
    min_accuracy = np.minimum(np.min(accuracy),np.min(val_accuracy))
    lower_limit = np.divide(np.floor(np.multiply(min_accuracy,50)),50) #nearest 2% below min accuracy on train or val

    plt.plot(epochs, accuracy, 'r', label='Training accuracy')
    plt.plot(epochs, val_accuracy, 'b', label='Validation accuracy')
    plt.title('Training and Validation Accuracy')
    plt.legend(loc=0)
    plt.grid()
    plt.xlim(1,len(accuracy)+1)
    plt.ylim(lower_limit,1)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.savefig(filename, dpi=300)
    plt.close()

def plot_loss_save(loss, val_loss, filename):
    '''
    Saves plot of training and validation loss vs epoch
    '''
    epochs = range(1,len(loss)+1)
    max_loss = np.maximum(np.max(loss),np.max(val_loss))
    upper_limit = np.divide(np.ceil(np.multiply(max_loss,50)),50) #nearest 2% above max loss on train or val

    plt.plot(epochs, loss, 'r', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and Validation Loss')
    plt.legend(loc=2)
    plt.grid()
    plt.xlim(1,len(loss)+1)
    plt.ylim(0,max_loss)
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.savefig(filename, dpi=300)
    plt.close()

class SpecLayer(tf.keras.layers.Layer):
    ''' A keras layer for creating spectrogram features.
    log_spec is 'MIR' method, not true log spacing in LF
    custom_log_spec is true log spacing.
    mag_phase_or_both is only relevant for custom_log_spec.
    Args:
        feature parameters - dictionary of parameters required
        expand_dims - whether or not to expand a final dimension for convolutional neural network channels (not used by custom_log_spec) required as variable as False for full_channel_class_bgru_dnn_bayesian
        triangular_filters - [bins, bands] filter bank of triangular smoothing
    Output:
        spectrogram features
    '''
    def __init__(self, feature_parameters={'feature_type' : 'efficient_custom_log_spec', 'mag_phase_or_both' : 'both', 'to_zero_pad' : True, 'bands_per_octave': 3.0}, triangular_filters=None, **kwargs):
        super().__init__(**kwargs)
        assert feature_parameters['feature_type'] in ('custom_log_spec', 'efficient_custom_log_spec', 'mfcc'), 'Invalid feature type'

        self.feature_parameters = feature_parameters
        self.log_filters_triangular = triangular_filters

    def call(self, input):
        if self.feature_parameters['feature_type'] == 'custom_log_spec':
            specs = tf_custom_log_spec(input, triangular_filters=self.log_filters_triangular, mag_phase_or_both=self.feature_parameters['mag_phase_or_both'], to_zero_pad=self.feature_parameters['to_zero_pad'])
        elif self.feature_parameters['feature_type'] == 'efficient_custom_log_spec':
            specs = efficient_custom_log_spec(input, mag_phase_or_both=self.feature_parameters['mag_phase_or_both'], to_zero_pad=self.feature_parameters['to_zero_pad'], bands_per_octave=self.feature_parameters['bands_per_octave'])
        else:
            specs = mfcc(input, custom_stft=self.feature_parameters['custom_stft'], mel_output=self.feature_parameters['mel_output'])
        return specs

@tf.function
def tf_audioread(filename, n_channels=1):
    '''
    Loads wav file as numpy array using tensorflow.
    Must be 16-bit.
    '''
    wav_loader = io_ops.read_file(filename)
    wav_decoder = tf.audio.decode_wav(wav_loader, desired_channels=n_channels) # can pass desired_samples. pcm float between -1 and 1
    wav_decoder = [tf.squeeze(i).numpy() for i in wav_decoder]
    return wav_decoder


@tf.function
def tf_log_base(x, base=10):
    '''
    Calculates log of x with given base.
    '''
    numerator = tf.math.log(x)
    denominator = tf.math.log(tf.constant(base, dtype=numerator.dtype))
    return tf.cast(tf.divide(numerator, denominator), dtype="float32")


@tf.function
def tf_log_frequencies_base10(fmin=20.0, fmax=20000.0, fref=1000.0, bands_per_octave=12.0):
    """
    Calculate log frequencies using tensorflow - BS EN 61260-1:2014.
    All returned frequenceis are within the bounds fmin - fmax.

    """
    G = 10.0**(3.0/10.0)
    lower_index = tf.math.ceil(tf_log_base(fmin/fref, G)*bands_per_octave)
    upper_index = tf.math.ceil(tf_log_base(fmax/fref, G)*bands_per_octave)
    index = tf.range(lower_index, upper_index) 
    fc = fref * G ** (tf.cast(index,tf.float64)/bands_per_octave)
    f_upper = fc * G **(1/(bands_per_octave))
    f_lower = fc * G **(-1/(bands_per_octave))

    return f_lower, fc, f_upper


def np_log_frequencies_base10(fmin=20.0, fmax=20000.0, fref=1000.0, bands_per_octave=12.0):
    """
    Calculate log frequencies using tensorflow - BS EN 61260-1:2014.
    All returned frequenceis are within the bounds fmin - fmax.
    """
    G = 10.0**(3.0/10.0)
    lower_index = np.math.ceil(np.math.log(fmin/fref, G)*bands_per_octave)
    upper_index = np.math.ceil(np.math.log(fmax/fref, G)*bands_per_octave)
    index = np.arange(lower_index, upper_index)
    fc = fref * G ** (index/bands_per_octave)
    f_upper = fc * G **(1/(bands_per_octave))
    f_lower = fc * G **(-1/(bands_per_octave))

    return f_lower, fc, f_upper


@tf.function
def tf_fvec(frame_len, sample_rate):
    '''
    Create single sided frequency vector, up to but not including Nyquist frequency.
    '''
    fft_length = tf.cast(frame_len, tf.float32)
    freq_resolution = sample_rate / fft_length 
    nfeqs =  tf.cast(fft_length / 2.0, tf.int32)
    fvec_ss = tf.cast(tf.range(0, nfeqs), tf.float32) * freq_resolution
    return fvec_ss


def np_fvec(frame_len, sample_rate):
    '''
    Create single sided frequency vector, up to but not including Nyquist frequency.
    '''
    fft_length = frame_len
    freq_resolution = sample_rate / fft_length
    nfeqs =  fft_length // 2.0
    fvec_ss = np.arange(0, nfeqs) * freq_resolution
    return fvec_ss


@tf.function
def tf_log_frequencies(fmin=20.0, fmax=20000.0, fref=1000.0, bands_per_octave=12.0):
    """
    Calculate log-spaced frequencies using tensorflow.
    Includes lower and upper frequencies for start and end of triangular smoothing filters.
    These are spaced at 2x given octave resolution so -6 dB points are spaced at given octave resolution.
    """
    minimum = tf.floor(tf.multiply(tf.divide(tf.math.log(tf.divide(fmin, fref)), tf.math.log(2.0)), bands_per_octave))
    maximum = tf.math.ceil(tf.multiply(tf.divide(tf.math.log(tf.divide(fmax, fref)), tf.math.log(2.0)), bands_per_octave))
    centre_freqs = tf.slice(tf.multiply(fref, tf.pow(2.0, tf.divide(tf.range(minimum, maximum), bands_per_octave))), [1], [-1])
    lower_freqs = tf.multiply(centre_freqs, tf.pow(2.0, tf.divide(-1, bands_per_octave)))
    upper_freqs = tf.multiply(centre_freqs, tf.pow(2.0, tf.divide(1, bands_per_octave)))
    return lower_freqs, centre_freqs, upper_freqs


def np_get_triangular_log_filters(fmin=20.0, fmax=20000.0, fref=1000.0, bands_per_octave=12.0, sample_rate=44100, frame_size=1024):
    '''
    Creates matrix of triangular for log smoothing, in a customised improved way
    '''
    _, fc, _ = np_log_frequencies_base10(fmin=fmin, fmax=fmax, fref=fref, bands_per_octave=bands_per_octave)
    fvec_ss = np_fvec(frame_size, sample_rate)
    log_filters_triangular = np_trangular_filters(fvec_ss, fc)
    return tf.transpose(log_filters_triangular)


def np_trangular_filters(fft_freqs, fcs):
    '''
    Computes triangular filters, only required once
    '''
    filters = []
    mask = np.zeros([fcs.shape[0], fft_freqs.shape[0]]) # filters to be populated
    fft_freqs_np = np.array(fft_freqs)
    for bandi in range(1, fcs.shape[0]-1):
        fc = fcs[bandi]   # centre freq of band
        fl = fcs[bandi-1] # lower edge
        fu = fcs[bandi+1] # higher edge
        # gradient of rising edge
        gradient = 1 / (fc-fl)
        # find where to apply rising edge
        Il = np.where(np.logical_and(fft_freqs_np>fl, fft_freqs_np<fc))
        # create line
        mask[bandi, Il] = gradient * fft_freqs_np[Il] + (-gradient * fl)
        # find where to apply falling edge
        Iu = np.where(np.logical_and(fft_freqs_np>=fc, fft_freqs_np<fu))
        # gradient of falling edge
        gradient = 1 / (fc-fu)
        # create line
        mask[bandi, Iu] = gradient * fft_freqs_np[Iu] + (-gradient * fu)

    return tf.cast(tf.constant(mask), tf.float32)


@tf.function
def log_smooth(log_filters_triangular, lin_spec_custom):
    '''
    Log smoothing using preprocessed triangular log filters
    log_filters_triangular generated using tf_get_triangular_log_filters or np_get_triangular_log_filters.
    lin_spec_custom generated using tf_custom_stft (can be a batch of audio files.)
    This will be soon superseded by function which both calculated filters and applied in a more efficient manner.
    The three below methods are equivelant in both under-the-hood execution and runtime.
    lin_spec_custom has dimensionality [instances, frames, bins, mag/phase].
    log_filters_triangular has dimensionality [bins, bands].
    '''
    # Matmul method (requires logic for mag_phase_or_both)
    # log_mag = tf.matmul(lin_spec_custom[:, :, :, 0], log_filters_triangular)
    # log_phase = tf.matmul(lin_spec_custom[:, :, :, 1], log_filters_triangular)
    # log_magnitude_and_phase = tf.stack((log_mag, log_phase), -1)

    # Einstein summation method (calls matmul under the hood) (requires logic for mag_phase_or_both)
    # log_mag = tf.einsum('ijk, kb->ijb', lin_spec_custom[:, :, :, 0] , log_filters_triangular)
    # log_phase = tf.einsum('ijk, kb->ijb', lin_spec_custom[:, :, :, 1] , log_filters_triangular)
    # log_magnitude_and_phase = tf.stack((log_mag, log_phase), -1)

    # Single matmul method (has benifit of not requiring logic for mag_phase_or_both)
    lin_spec_custom_perm = tf.transpose(lin_spec_custom, perm=[0,1,3,2])
    log_magnitude_and_phase_perm = tf.matmul(lin_spec_custom_perm, log_filters_triangular)
    log_magnitude_and_phase = tf.transpose(log_magnitude_and_phase_perm, perm=[0,1,3,2])

    return log_magnitude_and_phase


@tf.function
def tf_custom_stft(audio, frame_size=2048, hop_size=256, mag_phase_or_both='mag', unwrap_phase=True, window_fn=tf.signal.hann_window, to_zero_pad=False, ):
    '''
        Performs stft on audio, then calculates magnitude and phase (wrapped or unwrapped)
        Stacks the output into dimensionality [instances, frames, frequency bins, magnitude_and_or_phase]

    args:
        audio: list of filenames or np array audio data
    '''
    half_frame_size = tf.cast(tf.divide(frame_size, 2), "float32")

    audio = tf.map_fn(lambda x: tf.divide(x, tf.math.reduce_max(tf.math.abs(x))), audio)    

    if to_zero_pad:
        stfts = tf.slice(tf.signal.stft(audio, frame_size, hop_size, fft_length=32768, window_fn=window_fn, pad_end=True), [0, 0, 0], [-1, -1, 16384])
    else:
        stfts = tf.slice(tf.signal.stft(audio, frame_size, hop_size, fft_length=frame_size, window_fn=window_fn, pad_end=True), [0, 0, 0], [-1, -1, half_frame_size])

    if mag_phase_or_both == 'both':
        # stack magnitude and phase
        magnitude = tf.abs(stfts)
        phase = tf.cast(tf.math.angle(stfts), dtype="float32")
        if unwrap_phase:
            phase_unwrapped = tf_unwrap(phase, to_correct=True)
            magnitude_and_phase = tf.stack((magnitude, phase_unwrapped), -1)
            return magnitude_and_phase
        else:
            magnitude_and_phase = tf.stack((magnitude, phase), -1)
            return magnitude_and_phase
    elif mag_phase_or_both == 'mag':
        # magnitude
        magnitude = tf.abs(stfts)
        magnitude = tf.expand_dims(magnitude, -1)
        return magnitude
    else:
        # phase
        phase = tf.cast(tf.math.angle(stfts), dtype="float32")
        magnitude = tf.expand_dims(magnitude, -1)
        return phase

@tf.function
def tf_custom_log_spec(audio, triangular_filters, mag_phase_or_both='both', to_zero_pad=False):
    """
    Calculate a log spectrogram including phase using tensorflow
    Args:
        audio: list of audio data to be processed
        log_filters_triangular: log smoothing triangular filters, created (once only) by trangular_filters

    Returns:
        log_spectrograms: dimensionality [instances, frames, frequency bands, magnitude_and_phase]

    """
    lin_spec_custom = tf_custom_stft(audio, mag_phase_or_both=mag_phase_or_both, to_zero_pad=to_zero_pad) # shape [instances, frames, bins, mag/phase]
    log_spec_custom = log_smooth(triangular_filters, lin_spec_custom) # shape [instances, frames, bands, mag/phase]
    return log_spec_custom

@tf.function
def efficient_custom_log_spec(audio, mag_phase_or_both='both', to_zero_pad=False, bands_per_octave=12.0):
    """
    Calculate a log spectrogram including phase using tensorflow
    Args:
        audio: list of audio data to be processed

    Returns:
        log_spectrograms: dimensionality [instances, frames, frequency bands, magnitude_and_phase]

    """
    lin_spec_custom = tf_custom_stft(audio, mag_phase_or_both=mag_phase_or_both, to_zero_pad=to_zero_pad) # shape [instances, frames, bins, mag/phase]
    log_spec_custom = calculate_and_apply_smoothing_filters(bands_per_octave=bands_per_octave, lin_spec=lin_spec_custom, to_zero_pad=to_zero_pad) # shape [instances, frames, bands, mag/phase]
    return log_spec_custom

@tf.function
def mfcc(audio, custom_stft=True, to_zero_pad=False, lower_edge_hertz=80.0, upper_edge_hertz=7600.0, num_mel_bins=80, sample_rate=16000.0, mel_output='mfccs'):
    if custom_stft:
        stfts = tf_custom_stft(audio, mag_phase_or_both='mag', to_zero_pad=True)
        magnitude = tf.squeeze(stfts, axis=-1)
    else:
        stfts = tf.abs(tf.signal.stft(audio, frame_length=2048, frame_step=256, fft_length=2048))
        magnitude = tf.abs(stfts)
    # Warp the linear scale spectrograms into the mel-scale.
    num_spectrogram_bins = magnitude.shape[-1]
    linear_to_mel_weight_matrix = tf.signal.linear_to_mel_weight_matrix(
                                                                        num_mel_bins, 
                                                                        num_spectrogram_bins, 
                                                                        sample_rate, 
                                                                        lower_edge_hertz,
                                                                        upper_edge_hertz)

    mel_spectrograms = tf.tensordot(magnitude, linear_to_mel_weight_matrix, 1)
    mel_spectrograms.set_shape(stfts.shape[:-1].concatenate(linear_to_mel_weight_matrix.shape[-1:]))

    # Compute a stabilized log to get log-magnitude mel-scale spectrograms.
    log_mel_spectrograms = tf.math.log(mel_spectrograms + 1e-6)

    if mel_output == 'mfccs':
        # Compute MFCCs from log_mel_spectrograms and take the first 13.
        mfccs = tf.signal.mfccs_from_log_mel_spectrograms(log_mel_spectrograms)[..., :13]
        return mfccs
    else:
        return log_mel_spectrograms

@tf.function
def smooth_band(fl_band, fc_band, fu_band, fft_freqs, lin_spec_custom):
    '''
    Efficiently smoothes using a triangular filter for a given log spaced band, defined by:
    fl_band: lower frequency of band
    fc_band: centre frequency of band
    fu_band: upper frequency of band
    Uses:
    fft_freqs: linear spaces single sided frequency vector
    lin_spec_custom: linearly spaced specrogram of dimensionality [instances, frames, frequency bins, magnitude_and_or_phase]
    '''
    mask_rising = ((fft_freqs>fl_band) & (fft_freqs<fc_band))
    mask_falling = ((fft_freqs>=fc_band)&(fft_freqs<fu_band))

    relevant_lin_spec_custom_rising = tf.boolean_mask(lin_spec_custom, mask_rising, axis=2)
    relevant_lin_spec_custom_falling = tf.boolean_mask(lin_spec_custom, mask_falling, axis=2)
    relevant_lin_spec_custom = tf.concat((relevant_lin_spec_custom_rising,relevant_lin_spec_custom_falling), axis=2)

    fft_freqs_rising = tf.boolean_mask(fft_freqs, mask_rising)
    fft_freqs_falling = tf.boolean_mask(fft_freqs, mask_falling)

    # gradient rising band and falling band:
    m1 = 1 / (fc_band-fl_band)
    m2 = 1 / (fc_band-fu_band)

    # constants:
    c1 = -fl_band * m1
    c2 = -fu_band * m2

    # y = mx+c
    rising_values_band = m1 * fft_freqs_rising + c1 
    falling_values_band = m2 * fft_freqs_falling + c2

    band_triangle = tf.expand_dims(tf.concat((rising_values_band, falling_values_band), 0),-1)

    lin_spec_custom_perm = tf.transpose(relevant_lin_spec_custom, perm=[0,1,3,2])
    log_magnitude_and_phase_perm = tf.matmul(lin_spec_custom_perm, band_triangle)
    log_magnitude_and_phase = tf.transpose(log_magnitude_and_phase_perm, perm=[0,1,3,2]) #this is target orientation
    log_magnitude_and_phase_perm2 = tf.transpose(log_magnitude_and_phase, perm=[2,1,0,3]) # swapped so it stacks in first dimension
    # the above two can be done in one line with some thought
    log_magnitude_and_phase_transformed = tf.squeeze(log_magnitude_and_phase_perm2, axis=0)
    # shape=(28, 63, 512, 2) ??
    return log_magnitude_and_phase_transformed

@tf.function
def calculate_and_apply_smoothing_filters(fmin=20.0, fmax=20000.0, fref=1000.0, bands_per_octave=12.0, frame_size=2048, sample_rate=44100, lin_spec=None, to_zero_pad=True):
    '''
    Uses smooth_band to efficiently log smooth using triangular filters.
    lin_spec_custom: linearly spaced specrogram of dimensionality [instances, frames, frequency bins, magnitude_and_or_phase]
    to_zero_pad: boolean. Greatly improves accuracy of LF at slight speed penalty
    '''
    _, fcs, _ = np_log_frequencies_base10(fmin=fmin, fmax=fmax, fref=fref, bands_per_octave=bands_per_octave) # all in TF
    if to_zero_pad:
        fft_freqs = tf_fvec(32768, sample_rate)
    else:
        fft_freqs = tf_fvec(frame_size, sample_rate)

    n_bands = tf.shape(fcs)[0]
    n_bins = tf.shape(fft_freqs)[0]

    ind_bands_minus_ends = tf.cast(tf.linspace(tf.cast(1, dtype="float32"), tf.cast(n_bands-2, dtype="float32"), n_bands-2), dtype = "int32")
    fc = tf.cast(fcs[1:-1], dtype = "float32")
    fl = tf.cast(fcs[0:-2], dtype = "float32")
    fu = tf.cast(fcs[2:], dtype = "float32")

    # loop over indicies of log spacing:
    smooth_band_fn = lambda x: smooth_band(fl[x], fc[x], fu[x], fft_freqs, lin_spec)
    log_spec_custom_perm = tf.map_fn(smooth_band_fn, ind_bands_minus_ends-1, dtype = tf.float32)
    log_spec_custom = tf.transpose(log_spec_custom_perm, perm=[2,1,0,3])
    return log_spec_custom

@tf.function
def tf_unwrap(phase, to_correct=True):
    '''
    Unwraps phase in vectorised manner.
    Unwraps phase to between +/- pi in the same manner as np.unwrap (when to_correct set to False.)
    to_correct ensures phase has overall negative gradient, ensuring causality.
    phase has dimensionality [instances, frames, frequency bins]
    '''
    difference = tf.subtract(phase[:,:,1:], phase[:,:,0:-1])
    phase_adder = tf.multiply(-1.0, tf.cast((difference > np.pi), dtype="float32"))
    phase_subtract = tf.multiply(1.0, tf.cast((difference < -np.pi), dtype="float32"))
    phase_difference = tf.math.cumsum(phase_adder + phase_subtract, axis=2) * 2.0 * np.pi
    zeros = tf.zeros((tf.shape(phase_difference)[0], tf.shape(phase_difference)[1], 1), dtype="float32")
    phase_unwrapped = tf.concat([zeros, phase_difference], 2) + phase
    multiplier = ((tf.cast(phase_unwrapped[:,:,-1] > phase_unwrapped[:,:,0], dtype="float32") * -1.0) * 2.0) + 1.0
    phase_unwrapped = tf.multiply(tf.expand_dims(multiplier,2), phase_unwrapped)

    return phase_unwrapped

def load_speech_commands_dataset(data_root, load_training=True, fraction_of_data=1, data_type='raw_audio'):
    t = time.time()
    # filepaths to text file of dataset split
    data_dir = os.path.join(data_root,'data')
    testing_txt = os.path.join(data_root,'testing_list.txt')
    validation_txt = os.path.join(data_root,'validation_list.txt')

    # generate list of filepaths for each dataset
    testing_list = list_from_txt(testing_txt)
    validation_list = list_from_txt(validation_txt)

    np.random.shuffle(validation_list)

    validation_list = validation_list[0:int(np.round(fraction_of_data*len(validation_list)))]

    # turn these into dictionaries separated by class
    test_filepath_dictionary = return_filepath_dictionary(testing_list)
    val_filepath_dictionary = return_filepath_dictionary(validation_list)

    # train label encoder on training set
    label_encoder = preprocessing.LabelEncoder()
    label_encoder.fit(list(val_filepath_dictionary.keys()))

    # extract input data X and labels y for each dataset
        
    print("extracting validation data...")
    X_val, y_val = generate_data_from_filepath_dictionary_multiprocessed(val_filepath_dictionary, data_dir, label_encoder, data_type='raw_audio')
    print("extracting test data...") 
    X_test, y_test = generate_data_from_filepath_dictionary_multiprocessed(test_filepath_dictionary, data_dir, label_encoder, data_type='raw_audio')

    X_val = tf.cast(tf.squeeze(X_val, axis=2), dtype="float32")
    y_val = tf.cast(y_val, dtype="int32")

    X_test = tf.cast(tf.squeeze(X_test, axis=2), dtype="float32")
    y_test = tf.cast(y_test, dtype="int32")
        
    if load_training:
        training_list = compile_training_list(data_dir, validation_list, testing_list)
        np.random.shuffle(training_list)
        training_list = training_list[0:int(np.round(fraction_of_data*len(training_list)))]
        train_filepath_dictionary = return_filepath_dictionary(training_list)
        print("extracting training data...")
        X_train, y_train = generate_data_from_filepath_dictionary_multiprocessed(train_filepath_dictionary, data_dir, label_encoder, data_type=data_type)
        X_train = tf.cast(tf.squeeze(X_train, axis=2), dtype="float32")
        y_train = tf.cast(y_train, dtype="int32")
        assert train_filepath_dictionary.keys() == val_filepath_dictionary.keys() == test_filepath_dictionary.keys(), "not all datasets contain all classes"
        time_it('data loading and preparation', t)
        print("Data loaded and ready to go!")
        return X_train, y_train, X_val, y_val, X_test, y_test
    else:
        assert val_filepath_dictionary.keys() == test_filepath_dictionary.keys(), "not all datasets contain all classes"
        time_it('data loading and preparation', t)
        print("Data loaded and ready to go!")
        return X_val, y_val, X_test, y_test

def evaluate_model(model, X=None, y=None, print_results=True):

    logits = model.predict(X, use_multiprocessing=True, verbose=1)
    y_predicted = tf.argmax(logits, axis=1)

    MCC = matthews_corrcoef(y, y_predicted)
    accuracy = accuracy_score(y, y_predicted)
    accuracy_balanced = balanced_accuracy_score(y, y_predicted)
    f1 = f1_score(y, y_predicted, average='macro')
    precision = precision_score(y, y_predicted, average='macro')
    recall = recall_score(y, y_predicted, average='macro')
    cm = confusion_matrix(y, y_predicted)

    if print_results:
        print('\nMCC: {}'.format(MCC))
        print('accuracy: {}'.format(accuracy))
        print('accuracy_balanced: {}'.format(accuracy_balanced))
        print('f1_score: {}'.format(f1))
        print('precision: {}'.format(precision))
        print('recall: {}'.format(recall))

    # include dictionary of results for saving
    # make separate save results function
    # add saving of coloured plots here

def export_json(filename, dictionary):
    '''
    Convert dictionary to json format and export.
    '''
    with open(filename, 'w') as outfile:
        json.dump(dictionary, outfile, indent=4)
