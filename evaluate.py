import tensorflow as tf
import os
from utils import *
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, balanced_accuracy_score, matthews_corrcoef, recall_score, precision_score, confusion_matrix, f1_score

# Load model
saved_model_filepath = '/Users/andyharper/Developer/lab/speech_recognition/saved_models/speech_recognition_21'
model = tf.keras.models.load_model(saved_model_filepath)

# Load data
data_root = '/Users/andyharper/Documents/Datasets/speech_commands_v0.02/'
X_val, y_val, X_test, y_test = load_speech_commands_dataset(data_root, load_training=False, fraction_of_data=1, data_type='raw_audio')

# Predict on model with data and evaluate results
print("\n"+saved_model_filepath.split('/')[-1]+":")
evaluate_model(model, X=X_val, y=y_val)
