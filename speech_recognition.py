import os
from utils import *
from sklearn import preprocessing
import numpy as np
import tensorflow as tf
import time
from tensorflow.keras.layers import Input, Conv1D, BatchNormalization, LeakyReLU, Dropout, GRU, Flatten, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2
# from tensorflow.python.framework.ops import disable_eager_execution
# disable_eager_execution()

name = __file__
directory, model_name = os.path.split(name[:-3])
data_root = '/Users/andyharper/Documents/Datasets/speech_commands_v0.02/'

X_train, y_train, X_val, y_val, X_test, y_test = load_speech_commands_dataset(data_root, load_training=True, fraction_of_data=1, data_type='raw_audio')

(m_train, n_inputs_dim1) = X_train.shape
m_val = X_val.shape[0]
reg_lambda_range = np.logspace(np.log10(0.0001), np.log10(0.02), 5)

model_parameters={
                    'model_name': model_name,
                    'model_type': '1dcrn',
                    'feature_type' : 'mfcc',
                    'mel_output': 'mfccs',
                    'custom_stft' : False,
                    'to_zero_pad' : False,
                    'return_sequences': True,
                    'n_classes': 35,
                    'reg_lambda': reg_lambda_range[0],
                    'dropout': 0.2,
                    'recurrant_units': 196,
                    'recurrant_activation': 'tanh',
                    'conv_kernals': 128,
                    'conv_filter_size': 15,
                    'conv_strides': 1,
                    'conv_activation': 'relu',
                    'n_hidden': [256],
                    'dense_activation': 'relu',
                    }
# Training setup
BATCH_SIZE = 128
NUM_EPOCHS = 500
adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
reg = l2(model_parameters['reg_lambda'])

# Model
input_layer = Input(shape=(n_inputs_dim1), name='X')
X = SpecLayer(feature_parameters=model_parameters)(input_layer)
X = Conv1D(
            model_parameters['conv_kernals'], 
            model_parameters['conv_filter_size'], 
            strides=model_parameters['conv_strides'], 
            activation=model_parameters['conv_activation'],
            )(X)
X = BatchNormalization()(X)
X = LeakyReLU()(X)
X = Dropout(model_parameters['dropout'], noise_shape=(None, 1, model_parameters['conv_kernals']))(X)
X = GRU(units = model_parameters['recurrant_units'], return_sequences=True, activation=model_parameters['recurrant_activation'])(X)
X = Dropout(model_parameters['dropout'], noise_shape=(None, 1, model_parameters['recurrant_units']))(X)
X = BatchNormalization()(X)
X = GRU(units = model_parameters['recurrant_units'],
        return_sequences = model_parameters['return_sequences'],
        dropout=model_parameters['dropout'],
        activation=model_parameters['recurrant_activation'])(X)
X = BatchNormalization()(X)
X = Flatten()(X)
if model_parameters['n_hidden']:
    X = Dropout(model_parameters['dropout'])(X)
    X = Dense(model_parameters['n_hidden'][0], activation=model_parameters['dense_activation'], kernel_regularizer=reg)(X)
X = Dropout(model_parameters['dropout'])(X)
logits = Dense(model_parameters['n_classes'], activation=model_parameters['dense_activation'])(X) # time distributed if you have labels per frame
model = tf.keras.Model(inputs=input_layer, outputs=logits) 
model.summary()

model.compile(loss='sparse_categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

# callbacks - LR plateau won't be on until early stopping patience exceed LR patience
to_lr_plateau = False
callbacks = [
    tf.keras.callbacks.EarlyStopping(
        min_delta=1e-3,
        patience=5,
        verbose=1,
        restore_best_weights = True)]

if to_lr_plateau:
    callbacks.append(
                        tf.keras.callbacks.ReduceLROnPlateau(
                            factor=0.5,
                            patience=4,
                            min_lr=0.5e-5,
                            verbose=1)
                    )

history = model.fit(
                    x=X_train,
                    y=y_train,
                    batch_size=BATCH_SIZE,
                    callbacks=callbacks,
                    epochs=NUM_EPOCHS,
                    validation_data = (X_val, y_val)
                    )

evaluate_model(model, X=X_val, y=y_val)

tf.keras.models.save_model(model, os.path.join('/Users/andyharper/Developer/lab/speech_recognition/saved_models', model_name))
tf.saved_model.save(model, os.path.join(directory, 'saved_models', model_name))
converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.allow_custom_ops=True
converter.target_spec.supported_ops = set([tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS])
optimization = tf.lite.Optimize.DEFAULT
converter.optimizations = [optimization]
tflite_model = converter.convert()
models_dir = os.path.join(directory, 'tflite')
os.makedirs(models_dir, exist_ok=True)
open(os.path.join(models_dir, model_name+'.tflite'), "wb").write(tflite_model)

# Save model parameters
model_parameter_dir = os.path.join(directory, 'model_parameters')
os.makedirs(model_parameter_dir, exist_ok=True)
export_json(os.path.join(model_parameter_dir, model_name), model_parameters)

# Plotting
plot_accuracy_save(history.history['accuracy'], history.history['val_accuracy'], os.path.join(directory,'plots',model_name+'_accuracy.png'))
plot_loss_save(history.history['loss'], history.history['val_loss'], os.path.join(directory,'plots',model_name+'_loss.png'))
